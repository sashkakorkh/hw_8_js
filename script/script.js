/*Exercise 1*/
/*1.Document Object Model (DOM) це модель документу, яку утворює браузер при читанні розмітки та коду.
Модель схожа на дерево з гілками, гілки це об'єкти розмітки в документі, які ми можемо змінювати за допомогою JS динамічно

 2.  innerHTML ми можемо використовувати текст з тегами, які будуть сприйматися не як просто текст,
 а як частина документу, innerText це чисто текст, теги.

 3. getElementById
    getElementsByName
    getElementsByClassName
    getElementsByTagName
    getElementsByAttribute
    querySelector
    querySelectorAll
    closest().
    Найбільш популярні getElementById, querySelector. Обидва способи майже однакові, окрім механізму,
     getElementById шукає елемент за Idб тому цей елемент буде унікальним на весь документ.
     querySelector може обрати елемент за селектором css, але це буде перший зі списку з однаковим селектором.
      Вибір способу залежить від того, що ми хочемо отримати.
    Якщо потрібна колекція елементів, то це getElementsBy(...)
    */


/*Exercise 2*/

let paragraphs = document.querySelectorAll('p');
 for(let parag of paragraphs) {
     console.log(parag)
     parag.style.backgroundColor = "#ff0000";
 }



 let optionList = document.getElementById('optionsList');
console.log(optionList)
let optionListParent = optionList.parentNode
console.log(optionListParent)


function hasChilds () {
    if (optionList.hasChildNodes()) {
        let optionListChilds = optionList.childNodes;
        for (let child of optionListChilds) {
            console.log(child.nodeName);
            console.log(child.nodeType);
        }
    }
        }
        hasChilds()


let testParagraph = document.querySelector('#testParagraph');
testParagraph.innerHTML = 'This is a paragraph';

let mainHeader = document.querySelector('.main-header');
let mainHeaderChildren = mainHeader.children;
console.log(mainHeaderChildren)
for (let headerChild of mainHeaderChildren ) {
    headerChild.classList.add('nav-item');
}

let sectionTitle = document.querySelectorAll('.section-title');
for(let title of sectionTitle) {
    title.classList.remove('section-title');
}

